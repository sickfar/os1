
bitflags! {
    pub struct MBInfoFlags: u32 {
        const INFO_MEMORY = 1;
        const BOOTDEV = 1 << 1;
        const CMDLINE = 1 << 2;
        const MODS = 1 << 3;
        //start of useless
        const AOUT_SYMS = 1 << 4;
        const ELF_SHDR = 1 << 5;
        //end of useless
        const MEM_MAP = 1 << 6;
        const DRIVE_INFO = 1 << 7;
        const CONFIG_TABLE = 1 << 8;
        const BOOT_LOADER_NAME = 1 << 9;
        const AMP_TABLE = 1 << 10;
        const VBE_INFO = 1 << 11;
        const FRAMEBUFFER_INFO = 1 << 12;
    }
}

#[repr(C, packed)]
#[derive(Copy, Clone)]
pub struct MultibootInfo {
    //according to spec
    flags: u32,

    //INFO_MEMORY
    mem_lower: u32,
    mem_upper: u32,

    //BOOTDEV
    boot_device: u32,

    //CMDLINE
    cmdline: u32,

    //MODS
    mods_count: u32,
    mods_addr: u32,

    //AOUT_SYMS && ELF_SHDR - one of
    aout_tabsize_elf_num: u32,
    aout_strsize_elf_size: u32,
    aout_addr_elf_addr: u32,
    aout_reserved_elf_ahndx: u32,

    //MEM_MAP
    mmap_lenght: u32,
    mmap_addr: u32,

    //DRIVE_INFO
    drives_lenght: u32,
    drives_addr: u32,

    //CONFIG_TABLE
    config_table: u32,

    //BOOT_LOADER_NAME
    boot_loader_name: u32,

    //AMP_TABLE
    amp_table: u32,

    //VBE_INFO
    vbe_control_info: u32,
    vbe_mode_info: u32,
    vbe_mode: u16,
    vbe_interface_seg: u16,
    vbe_interface_off: u16,
    vbe_interface_len: u16,
       
    //FRAMEBUFFER_INFO
    framebuffer_type: u8,
    framebuffer_info: FramebufferInfo,
}

impl MultibootInfo {
    pub fn flags(&self) -> Option<MBInfoFlags> {
        MBInfoFlags::from_bits(self.flags)
    }

    pub unsafe fn get_mmap(&self, index: usize) -> Option<*const MemMapEntry> {
        use crate::arch::get_mb_pointer_base;
        let base: usize = get_mb_pointer_base(self.mmap_addr as usize);
        let mut iter: *const MemMapEntry = (base as u32 + self.mmap_addr) as *const MemMapEntry;
        for _i in 0..index {
            iter = ((iter as usize) + ((*iter).size as usize) + 4) as *const MemMapEntry;
            if ((iter as usize) - base) >= (self.mmap_addr + self.mmap_lenght) as usize {
                return None
            } else {}
        }
        Some(iter)
    }
}

#[repr(C, packed)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct FrameBufferPaletteInfo {
    framebuffer_palette_addr: u32,
    framebuffer_palette_num_colors: u16
}

#[repr(C, packed)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct FrameBufferFieldsInfo {
    framebuffer_red_field_position: u8,
    framebuffer_red_mask_size: u8,
    framebuffer_green_field_position: u8,
    framebuffer_green_mask_size: u8,
    framebuffer_blue_field_position: u8,
    framebuffer_blue_mask_size: u8,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub union FramebufferInfo {
    palette: FrameBufferPaletteInfo,
    fields: FrameBufferFieldsInfo,
}

pub const MULTIBOOT_MEMORY_AVAILABLE: u32 = 1;
pub const MULTIBOOT_MEMORY_RESERVED: u32 = 2;
pub const MULTIBOOT_MEMORY_ACPI_RECLAIMABLE: u32 = 3;
pub const MULTIBOOT_MEMORY_NVS: u32 = 4;
pub const MULTIBOOT_MEMORY_BADRAM: u32 = 5;

#[repr(C, packed)]
#[derive(Copy, Clone)]
pub struct MemMapEntry {
    pub size: u32,
    pub addr: u64,
    pub len: u64,
    pub mtype: u32,
}

pub fn from_ptr<'a>(ptr: usize) -> &'a MultibootInfo {
    unsafe { &mut *(ptr as *mut MultibootInfo) }
}

pub fn parse_mmap(mbi: &MultibootInfo) {
    unsafe {
        let mut mmap_opt = mbi.get_mmap(0);
        let mut i: usize = 1;
        loop {
            let mmap = mmap_opt.unwrap();
            crate::memory::physical::map((*mmap).addr as usize, (*mmap).len as usize, translate_multiboot_mem_to_os1(&(*mmap).mtype));
            mmap_opt = mbi.get_mmap(i);
            match mmap_opt {
                None => break,
                _ => i += 1,
            }
        }
    }
}

pub fn translate_multiboot_mem_to_os1(mtype: &u32) -> usize {
    use crate::memory::physical::{RESERVED, UNUSABLE, USABLE};
    match mtype {
        &MULTIBOOT_MEMORY_AVAILABLE => USABLE,
        &MULTIBOOT_MEMORY_RESERVED => UNUSABLE,
        &MULTIBOOT_MEMORY_ACPI_RECLAIMABLE => RESERVED,
        &MULTIBOOT_MEMORY_NVS => UNUSABLE,
        &MULTIBOOT_MEMORY_BADRAM => UNUSABLE,
        _ => UNUSABLE
    }
}