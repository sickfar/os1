global arch_switch
global arch_start

section .text

arch_switch:
    cli
    ;pre-save
    push esp
    push ebp
    push eax
    push ebx
    push ecx
    push edx
    mov eax, [esp + 28] ;current
    mov edx, [esp + 32] ;next
    ;save
    pushfd
    pop ecx ;pushf
    mov [eax + 24], ecx ;flags
    pop ecx ;push edx
    mov [eax + 12], ecx
    pop ecx ;push ecx
    mov [eax + 8], ecx
    pop ecx ;push ebx
    mov [eax + 4], ecx
    pop ecx ;push eax
    mov [eax], ecx
    pop ecx ;push ebp
    mov [eax + 20], ecx
    pop ecx ;push esp
    mov [eax + 16], ecx
    ;restore
    mov ecx, [edx + 24]
    test ecx, ecx
    jz keep_flags
    push ecx
    popfd
keep_flags:
    mov esp, [edx + 16]
    mov ebp, [edx + 20]
    mov ebx, [edx + 4]
    mov ecx, [edx + 8]
    mov eax, [edx]
    mov edx, [edx + 12]
    sti
    ret


arch_start:
    mov eax, [esp + 4] ;current
    mov esp, [eax + 16]
    mov ebp, [eax + 20]
    mov ebx, [eax + 4]
    mov ecx, [eax + 8]
    mov edx, [eax + 12]
    mov eax, [eax]
    ret