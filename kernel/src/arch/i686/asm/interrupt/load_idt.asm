global load_idt
section .text

idtr dw 0 ; For limit storage
     dd 0 ; For base storage

load_idt:
    mov   eax, [esp + 4]
    mov   [idtr + 2], eax
    mov   ax, [esp + 8]
    mov   [idtr], ax
    lidt  [idtr]

    ret