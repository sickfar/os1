use crate::arch::io::port::{outb, outw, outd};

pub unsafe fn init() {
    outb(0x43, 0 | 0x30 | 5);
    outb(0x40, (2685 & 0xFF) as u8);
    outb(0x40, (2685 >> 8) as u8);
}