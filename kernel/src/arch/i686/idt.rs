pub static mut IDT: [IdtEntry; 256] = [IdtEntry::new(); 256];

const PIC1: u16 = 0x20;
const PIC2: u16 = 0xA0;

use crate::arch::io::port::outb;

extern {
    fn load_idt(base: *const IdtEntry, limit: u16);

    fn e0_zero_divide();
    fn e1_debug();
    fn e2_nmi();
    fn e3_brk_point();
    fn e4_overflow();
    fn e5_bound_range();
    fn e6_inv_opcode();
    fn e7_dev_not_avail();
    fn e8_double_fault();
    fn eA_inv_tss();
    fn eB_seg_not_present();
    fn eC_stack_seg_fault();
    fn eD_protection_fault();
    fn eE_page_fault();
    fn e10_x87_ex();
    fn e11_alignment_check();
    fn e12_machine_check();
    fn e13_simd_ex();
    fn e14_virtualization();
    fn e1E_security();

    fn irq0();
    fn irq1();
    fn irq2();
    fn irq3();
    fn irq4();
    fn irq5();
    fn irq6();
    fn irq7();
    fn irq8();
    fn irq9();
    fn irq10();
    fn irq11();
    fn irq12();
    fn irq13();
    fn irq14();
    fn irq15();
    fn int80();
}

unsafe fn setup_pic(pic1: u8, pic2: u8) {
    // Start initialization
	outb(PIC1, 0x11);
	outb(PIC2, 0x11);

	// Set offsets
	outb(PIC1 + 1, pic1);	/* remap */
	outb(PIC2 + 1, pic2);	/*  pics */

	// Set up cascade
	outb(PIC1 + 1, 4);	/* IRQ2 -> connection to slave */
	outb(PIC2 + 1, 2);

	// Set up interrupt mode (1 is 8086/88 mode, 2 is auto EOI)
	outb(PIC1 + 1, 1);
	outb(PIC2 + 1, 1);

	// Unmask interrupts
	outb(PIC1 + 1, 0);
    outb(PIC2 + 1, 0);

    // Ack waiting
    outb(PIC1, 0x20);
    outb(PIC2, 0x20);
}

pub unsafe fn init_idt() {
    IDT[0x0].set_func(e0_zero_divide);
    IDT[0x1].set_func(e1_debug);
    IDT[0x2].set_func(e2_nmi);
    IDT[0x3].set_func(e3_brk_point);
    IDT[0x4].set_func(e4_overflow);
    IDT[0x5].set_func(e5_bound_range);
    IDT[0x6].set_func(e6_inv_opcode);
    IDT[0x7].set_func(e7_dev_not_avail);
    IDT[0x8].set_func(e8_double_fault);
    IDT[0xA].set_func(eA_inv_tss);
    IDT[0xB].set_func(eB_seg_not_present);
    IDT[0xC].set_func(eC_stack_seg_fault);
    IDT[0xD].set_func(eD_protection_fault);
    IDT[0xE].set_func(eE_page_fault);
    IDT[0x10].set_func(e10_x87_ex);
    IDT[0x11].set_func(e11_alignment_check);
    IDT[0x12].set_func(e12_machine_check);
    IDT[0x13].set_func(e13_simd_ex);
    IDT[0x14].set_func(e14_virtualization);
    IDT[0x1E].set_func(e1E_security);

    IDT[0x20].set_func(irq0);
    IDT[0x21].set_func(irq1);
    IDT[0x22].set_func(irq2);
    IDT[0x23].set_func(irq3);
    IDT[0x24].set_func(irq4);
    IDT[0x25].set_func(irq5);
    IDT[0x26].set_func(irq6);
    IDT[0x27].set_func(irq7);
    IDT[0x28].set_func(irq8);
    IDT[0x29].set_func(irq9);
    IDT[0x2A].set_func(irq10);
    IDT[0x2B].set_func(irq11);
    IDT[0x2C].set_func(irq12);
    IDT[0x2D].set_func(irq13);
    IDT[0x2E].set_func(irq14);
    IDT[0x2F].set_func(irq15);

    IDT[0x80].set_func(int80);

    setup_pic(0x20, 0x28);

    let idt_ptr: *const IdtEntry = IDT.as_ptr();
    let limit = (IDT.len() * core::mem::size_of::<IdtEntry>() - 1) as u16;
    load_idt(idt_ptr, limit);
}

#[repr(C, packed)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct IdtEntry {
    addr_low: u16,
    selector: u16,
    zero: u8,
    type_flags: u8,
    addr_high: u16,
}

impl IdtEntry {
    pub const fn new() -> Self {
        IdtEntry {
            addr_low: 0,
            selector: 0,
            zero: 0,
            type_flags: 0,
            addr_high: 0,
        }
    }

    pub fn set_flags(&mut self, flags: IdtFlags) {
        self.type_flags = flags.bits;
    }

    pub fn set_offset(&mut self, selector: u16, base: usize) {
        self.selector = selector;
        self.addr_low = base as u16;
        self.addr_high = (base >> 16) as u16;
    }

    // A function to set the offset more easily
    pub fn set_func(&mut self, func: unsafe extern fn()) {
        self.set_flags(IdtFlags::PRESENT | IdtFlags::RING_0 | IdtFlags::INTERRUPT);
        self.set_offset(8, func as usize);
    }
}

bitflags! {
    pub struct IdtFlags: u8 {
        const STORAGE_SEGMENT = 1 << 4;
        const RING_0 = 0 << 5;
        const RING_1 = 1 << 5;
        const RING_2 = 2 << 5;
        const RING_3 = 3 << 5;
        const PRESENT = 1 << 7;
        const INTERRUPT = 0xE;
        const TRAP = 0xF;
    }
}