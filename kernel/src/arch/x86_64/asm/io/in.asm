global readb
global readw
global readd
section .text

readb:
    push ebp
    mov ebp, esp

    mov edx, [ebp + 8] ;load port argument to dx
    xor eax, eax ;clear
    in al, dx ;read byte by dx port to al

    mov esp, ebp
    pop ebp
    ret

readw:
    push ebp
    mov ebp, esp

    mov edx, [ebp + 8] ;load port argument to dx
    xor eax, eax ;clear
    in ax, dx ;read word by dx port to ax

    mov esp, ebp
    pop ebp
    ret

readd:
    push ebp
    mov ebp, esp

    mov edx, [ebp + 8] ;load port argument to dx
    xor eax, eax ;clear
    in eax, dx ;read double word by dx port to eax

    mov esp, ebp
    pop ebp
    ret