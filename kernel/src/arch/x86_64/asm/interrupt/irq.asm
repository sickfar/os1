global irq0
global irq1
global irq2
global irq3
global irq4
global irq5
global irq6
global irq7
global irq8
global irq9
global irq10
global irq11
global irq12
global irq13
global irq14
global irq15
 
extern kirq0
extern kirq1
extern kirq2
extern kirq3
extern kirq4
extern kirq5
extern kirq6
extern kirq7
extern kirq8
extern kirq9
extern kirq10
extern kirq11
extern kirq12
extern kirq13
extern kirq14
extern kirq15

section .text

irq0:
  pusha
  call kirq0
  popa
  iret
 
irq1:
  pusha
  call kirq1
  popa
  iret
 
irq2:
  pusha
  call kirq2
  popa
  iret
 
irq3:
  pusha
  call kirq3
  popa
  iret
 
irq4:
  pusha
  call kirq4
  popa
  iret
 
irq5:
  pusha
  call kirq5
  popa
  iret
 
irq6:
  pusha
  call kirq6
  popa
  iret
 
irq7:
  pusha
  call kirq7
  popa
  iret
 
irq8:
  pusha
  call kirq8
  popa
  iret
 
irq9:
  pusha
  call kirq9
  popa
  iret
 
irq10:
  pusha
  call kirq10
  popa
  iret
 
irq11:
  pusha
  call kirq11
  popa
  iret
 
irq12:
  pusha
  call kirq12
  popa
  iret
 
irq13:
  pusha
  call kirq13
  popa
  iret
 
irq14:
  pusha
  call kirq14
  popa
  iret
 
irq15:
  pusha
  call kirq15
  popa
  iret