extern {
    fn writeb(port: u16, value: u8);
    fn writew(port: u16, value: u16);
    fn writed(port: u16, value: u32);

    fn readb(port: u16) -> u8;
    fn readw(port: u16) -> u16;
    fn readd(port: u16) -> u32;
}

pub unsafe fn outb(port: u16, value: u8) {
    writeb(port, value);
}

pub unsafe fn outw(port: u16, value: u16) {
    writew(port, value);
}

pub unsafe fn outd(port: u16, value: u32) {
    writed(port, value);
}

pub unsafe fn inb(port: u16) -> u8 {
    readb(port)
}

pub unsafe fn inw(port: u16) -> u16 {
    readw(port)
}

pub unsafe fn ind(port: u16) -> u32 {
    readd(port)
}