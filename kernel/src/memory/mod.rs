pub mod allocate;
pub mod logical;
pub mod physical;

pub fn init() {
    physical::init();
    logical::init();
}

