//512 MiB should be enough for kernel heap. If not - ooops...
pub const KHEAP_START: usize = 0xE0000000;
//I will keep 1MiB info about my heap in separate 4MiB page before heap at this point
pub const KHEAP_INFO_ADDR: usize = 0xDFC00000;
pub const KHEAP_CHUNK_SIZE: usize = 64;

pub fn init() {
    KHEAP_INFO.lock().init();
}

#[repr(packed)]
#[derive(Copy, Clone)]
struct HeapPageInfo {
    //every bit represents 64 bytes chunk of memory. 0 is free, 1 is busy
    //u32 size is 4 bytes, so page information total size is 8KiB
    pub _4mb_by_64b: [u32; 2048],
}

impl HeapPageInfo {
    pub fn init(&mut self) {
        for i in 0..2048 {
            self._4mb_by_64b[i] = 0;
        }
    }

    pub fn mark_chunks_used(&mut self, _32pack: usize, chunk: usize, n: usize) {
        let mask: u32 = 0xFFFFFFFF >> (32 - n) << chunk;
        self._4mb_by_64b[_32pack] = self._4mb_by_64b[_32pack] | mask;
    }

    pub fn mark_chunks_free(&mut self, _32pack: usize, chunk: usize, n: usize) {
        let mask: u32 = 0xFFFFFFFF >> (32 - n) << chunk;
        self._4mb_by_64b[_32pack] = self._4mb_by_64b[_32pack] ^ mask;
    }

    pub fn empty(&self) -> bool {
        for i in 0..2048 {
            if self._4mb_by_64b[i] != 0 {
                return false
            }
        }
        true
    }
}

#[repr(packed)]
#[derive(Copy, Clone)]
struct HeapInfo {
    //Here we can know state of any 64 bit chunk in any of 128 4-MiB pages
    //Page information total size is 8KiB, so information about 128 pages requires 1MiB reserved data
    pub _512mb_by_4mb: [HeapPageInfo; 128],
}

impl HeapInfo {
    pub fn init(&mut self) {
        for i in 0..128 {
            self._512mb_by_4mb[i].init();
        }
    }

    // returns page number
    pub fn find_free_pages_of_size(&self, n: usize) -> usize {
        if n >= 128 {
            0xFFFFFFFF
        } else {
            let mut start_page: usize = 0xFFFFFFFF;
            let mut current_page: usize = 0xFFFFFFFF;
            for page in 0..128 {
                if self._512mb_by_4mb[page].empty() {
                    if current_page - start_page == n {
                        return start_page
                    }
                    if start_page == 0xFFFFFFFF {
                        start_page = page;
                    }
                    current_page = page;
                } else {
                    start_page = 0xFFFFFFFF;
                    current_page = 0xFFFFFFFF;
                }
            }
            0xFFFFFFFF
        }
    }

    // returns (page number, 32pack number)
    pub fn find_free_packs_of_size(&self, n: usize) -> (usize, usize) {
        if n < 2048 {
            for page in 0..128 {
                let mut start_pack: usize = 0xFFFFFFFF;
                let mut current_pack: usize = 0xFFFFFFFF;
                for _32pack in 0..2048 {
                    let _32pack_info = self._512mb_by_4mb[page]._4mb_by_64b[_32pack];
                    if _32pack_info == 0 {
                        if current_pack - start_pack == n {
                            return (page, start_pack)
                        }
                        if start_pack == 0xFFFFFFFF {
                            start_pack = _32pack;
                        }
                        current_pack = _32pack;
                    } else {
                        start_pack = 0xFFFFFFFF;
                        current_pack = 0xFFFFFFFF;
                    }
                }
            }
            (0xFFFFFFFF, 0xFFFFFFFF)
        } else {
            (0xFFFFFFFF, 0xFFFFFFFF)
        }
    }

    // returns (page number, 32pack number, chunk number)
    pub fn find_free_chunks_of_size(&self, n: usize) -> (usize, usize, usize) {
        if n < 32 {
            for page in 0..128 {
                for _32pack in 0..2048 {
                    let _32pack_info = self._512mb_by_4mb[page]._4mb_by_64b[_32pack];
                    let mask: u32 = 0xFFFFFFFF >> (32 - n);
                    for chunk in 0..(32-n) {
                        if ((_32pack_info >> chunk) & mask) ^ mask == mask {
                            return (page, _32pack, chunk)
                        }
                    }
                }
            }
            (0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF)
        } else {
            (0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF)
        }
    }

    fn mark_chunks_used(&mut self, page: usize, _32pack: usize, chunk: usize, n: usize) {
        self._512mb_by_4mb[page].mark_chunks_used(_32pack, chunk, n);
    }

    fn mark_chunks_free(&mut self, page: usize, _32pack: usize, chunk: usize, n: usize) {
        self._512mb_by_4mb[page].mark_chunks_free(_32pack, chunk, n);
    }

    fn mark_packs_used(&mut self, page: usize, _32pack:usize, n: usize) {
        for i in _32pack..(_32pack + n) {
            self._512mb_by_4mb[page]._4mb_by_64b[i] = 0xFFFFFFFF;
        }
    }

    fn mark_packs_free(&mut self, page: usize, _32pack:usize, n: usize) {
        for i in _32pack..(_32pack + n) {
            self._512mb_by_4mb[page]._4mb_by_64b[i] = 0;
        }
    }
}

use lazy_static::lazy_static;
use spin::Mutex;

lazy_static! {
    static ref KHEAP_INFO: Mutex<&'static mut HeapInfo> = 
        Mutex::new(unsafe { &mut *(KHEAP_INFO_ADDR as *mut HeapInfo) });
}

fn allocate_n_chunks_less_than_pack(n: usize, align: usize) -> *mut u8 {
    let mut heap_info = KHEAP_INFO.lock();
    let (page, _32pack, chunk) = heap_info.find_free_chunks_of_size(n);
    if page == 0xFFFFFFFF {
        core::ptr::null_mut()
    } else {
        let tptr: usize = KHEAP_START + 0x400000 * page + _32pack * 32 * 64 + chunk * 64;
        let res = tptr % align;
        let uptr = if res == 0 { tptr } else { tptr + align - res };
        //check bounds: more than start and less than 4GiB - 64B
        //but according to chunks error should never happen
        if uptr >= KHEAP_START && uptr <= 0xFFFFFFFF - 64 * n {
            heap_info.mark_chunks_used(page, _32pack, chunk, n);
            uptr as *mut u8
        } else {
            core::ptr::null_mut()
        }
    }
}

fn allocate_n_chunks_less_than_page(n: usize, align: usize) -> *mut u8 {
    let mut heap_info = KHEAP_INFO.lock();
    let packs_n: usize = n / 32;
    let lost_chunks = n - packs_n * 32;
    let mut packs_to_alloc = packs_n;
    if lost_chunks != 0 {
        packs_to_alloc += 1;
    }
    let (page, pack) = heap_info.find_free_packs_of_size(packs_to_alloc);
    if page == 0xFFFFFFFF {
        core::ptr::null_mut()
    } else {
        let tptr: usize = KHEAP_START + 0x400000 * page + pack * 32 * 64;
        let res = tptr % align;
        let uptr = if res == 0 { tptr } else { tptr + align - res };
        //check bounds: more than start and less than 4GiB - 64B
        //but according to chunks error should never happen
        if uptr >= KHEAP_START && uptr <= 0xFFFFFFFF - 64 * n {
            heap_info.mark_packs_used(page, pack, packs_n);
            if lost_chunks != 0 {
                heap_info.mark_chunks_used(page, pack + packs_to_alloc, 0, lost_chunks);
            }
            uptr as *mut u8
        } else {
            core::ptr::null_mut()
        }
    }
}

//unsupported yet
fn allocate_n_chunks_more_than_page(n: usize, align: usize) -> *mut u8 {
    let mut heap_info = KHEAP_INFO.lock();
    let packs_n: usize = n / 32;
    let lost_chunks = n - packs_n * 32;
    let mut packs_to_alloc = packs_n;
    if lost_chunks != 0 {
        packs_to_alloc += 1;
    }
    let pages_n: usize = packs_to_alloc / 2048;
    let mut lost_packs = packs_to_alloc - pages_n * 2048;
    let mut pages_to_alloc = pages_n;
    if lost_packs != 0 {
        pages_to_alloc += 1;
    }
    if lost_chunks != 0 {
        lost_packs -= 1;
    }
    let page = heap_info.find_free_pages_of_size(pages_to_alloc);
    if page == 0xFFFFFFFF {
        core::ptr::null_mut()
    } else {
        let tptr: usize = KHEAP_START + 0x400000 * page;
        let res = tptr % align;
        let uptr = if res == 0 { tptr } else { tptr + align - res };
        //check bounds: more than start and less than 4GiB - 64B * n
        //but according to chunks error should never happen
        if uptr >= KHEAP_START && uptr <= 0xFFFFFFFF - 64 * n {
            for i in page..(page + pages_n) {
                heap_info.mark_packs_used(i, 0, 2048);
            }
            if lost_packs != 0 {
                heap_info.mark_packs_used(page + pages_to_alloc, 0, lost_packs);
            }
            if lost_chunks != 0 {
                heap_info.mark_chunks_used(page + pages_to_alloc, lost_packs, 0, lost_chunks);
            }
            uptr as *mut u8
        } else {
            core::ptr::null_mut()
        }
    }
}

// returns pointer
pub fn allocate_n_chunks(n: usize, align: usize) -> *mut u8 {
    if n < 32 {
        allocate_n_chunks_less_than_pack(n, align)
    } else if n < 32 * 2048 {
        allocate_n_chunks_less_than_page(n, align)
    } else {
        allocate_n_chunks_more_than_page(n, align)
    }
}

pub fn free_chunks(ptr: usize, n: usize) {
    let page: usize = (ptr - KHEAP_START) / 0x400000;
    let _32pack: usize = ((ptr - KHEAP_START) - (page * 0x400000)) / (32 * 64);
    let chunk: usize = ((ptr - KHEAP_START) - (page * 0x400000) - (_32pack * (32 * 64))) / 64;
    let mut heap_info = KHEAP_INFO.lock();
    if n < 32 {
        heap_info.mark_chunks_free(page, _32pack, chunk, n);
    } else if n < 32 * 2048 {
        let packs_n: usize = n / 32;
        let lost_chunks = n - packs_n * 32;
        heap_info.mark_packs_free(page, _32pack, packs_n);
        if lost_chunks != 0 {
            heap_info.mark_chunks_free(page, _32pack + packs_n, 0, lost_chunks);
        }
    } else {
        let packs_n: usize = n / 32;
        let pages_n: usize = packs_n / 2048;
        let lost_packs: usize = packs_n - pages_n * 2048;
        let lost_chunks = n - packs_n * 32;
        for i in page..(page + pages_n) {
            heap_info.mark_packs_free(i, 0, 2048);
        }
        if lost_packs != 0 {
            heap_info.mark_packs_free(page + pages_n, 0, lost_packs);
        }
        if lost_chunks != 0 {
            heap_info.mark_chunks_free(page + pages_n, packs_n, 0, lost_chunks);
        }
    }
}