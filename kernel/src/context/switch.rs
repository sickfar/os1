use super::{Context, ContextState};
use alloc::boxed::Box;

static mut CURRENT: Option<Box<Context>> = None;

pub fn do_switch() {
    unsafe {
        let next_context: Option< Box<Context> > = super::schedule::pick();
        match next_context {
            None => {
                println!("No next context");
                return;
            },
            Some(mut next) => {
                let current: &mut Box<Context> = CURRENT.as_mut().unwrap();
                let current_ptr = &mut **current as *mut Context;
                let next_ptr = &mut *next as *mut Context;
                current.state = ContextState::Runnable;
                next.state = ContextState::Runnable;
                super::schedule::put(current);
                CURRENT = Some(next);
                crate::arch::context::switch(current_ptr, next_ptr);
            }
        }
    }
}

pub fn do_start(context: Context) {
    unsafe {
        CURRENT = Some(Box::new(context));
        let current: &mut Box<Context> = CURRENT.as_mut().unwrap();
        current.state = ContextState::Runnable;
        let current_arch = current.arch;
        let current_ptr = &mut **current;
        crate::arch::context::start(current_ptr);
    }
}